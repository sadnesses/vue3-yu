import {createRouter, createWebHashHistory} from "vue-router";
import Home from "../views/Home.vue";

const routes = [
    {
        path: '/',
        redirect: '/dashboard'
    }, {
        path: "/",
        name: "Home",
        component: Home,
        children: [
            {
                path: "/dashboard",
                name: "dashboard",
                meta: {
                    title: '系统首页',
                    noCache: true
                },
                component: () => import ( /* webpackChunkName: "dashboard" */ "../views/Dashboard.vue")
            }, {
                path: "/table",
                name: "basetable",
                meta: {
                    title: '接口权限',
                    noCache: true
                },
                component: () => import ( /* w	ebpackChunkName: "table" */ "../views/BaseTable.vue")
            }, {
                path: "/echarts",
                name: "yuEcharts",
                meta: {
                    title: 'Echarts图表',
                    noCache: true
                },
                component: () => import ( /* webpackChunkName: "charts" */ "../views/yuEcharts.vue")
            }, {
                path: "/form",
                name: "baseform",
                meta: {
                    title: '表单',
                    noCache: true
                },
                component: () => import ( /* webpackChunkName: "form" */ "../views/yuForm.vue")
            }, {
                path: "/tabs",
                name: "tabs",
                meta: {
                    title: 'tab标签',
                    noCache: true
                },
                component: () => import ( /* webpackChunkName: "tabs" */ "../views/Tabs.vue")
            }, {
                path: "/donate",
                name: "donate",
                meta: {
                    title: '鼓励作者',
                    noCache: true
                },
                component: () => import ( /* webpackChunkName: "donate" */ "../views/Donate.vue")
            }, {
                path: "/permission",
                name: "permission",
                meta: {
                    title: '权限管理',
                    permission: true
                },
                component: () => import ( /* webpackChunkName: "permission" */ "../views/Permission.vue")
            }, {
                path: "/i18n",
                name: "i18n",
                meta: {
                    title: '国际化语言',
                    noCache: true
                },
                component: () => import ( /* webpackChunkName: "i18n" */ "../views/I18n.vue")
            }, {
                path: "/upload",
                name: "upload",
                meta: {
                    title: '上传插件',
                    noCache: true
                },
                component: () => import ( /* webpackChunkName: "upload" */ "../views/Upload.vue")
            }, {
                path: "/icon",
                name: "icon",
                meta: {
                    title: '自定义图标',
                    noCache: true
                },
                component: () => import ( /* webpackChunkName: "icon" */ "../views/Icon.vue")
            }, {
                path: '/404',
                name: '404',
                meta: {
                    title: '找不到页面',
                    noCache: true
                },
                component: () => import (/* webpackChunkName: "404" */ '../views/404.vue')
            }, {
                path: '/403',
                name: '403',
                meta: {
                    title: '没有权限',
                    noCache: true
                },
                component: () => import (/* webpackChunkName: "403" */ '../views/403.vue')
            }, {
                path: '/user',
                name: 'user',
                meta: {
                    title: '个人中心',
                    noCache: true
                },
                component: () => import (/* webpackChunkName: "user" */ '../views/User.vue')
            }, {
                path: '/editor',
                name: 'editor',
                meta: {
                    title: '富文本编辑器',
                    noCache: true
                },
                component: () => import (/* webpackChunkName: "editor" */ '../views/Editor.vue')
            }, {
                path: '/bgdata',
                name: 'bigData',
                meta: {
                    title: '大数据demo',
                    noCache: true
                },
                component: () => import (/* webpackChunkName: "editor" */ '../views/bigData.vue')
            },
            {
                path: '/log',
                name: 'log',
                meta: {
                    title: '实时日志',
                    noCache: true
                },
                component: () => import (/* webpackChunkName: "editor" */ '../views/log.vue')
            },
            {
                path: '/editMarkDown',
                name: 'markdown',
                meta: {
                    title: 'markdown编辑器',
                    noCache: true
                },
                component: () => import (/* webpackChunkName: "editor" */ '../views/MarkDown.vue')
            }
        ]
    }, {
        path: "/login",
        name: "Login",
        meta: {
            title: '登录'
        },
        component: () => import ( /* webpackChunkName: "login" */ "../views/Login.vue")
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} | yu-vue3`;
    const username = sessionStorage.getItem('username');
    if (!username && to.path !== '/login') {
        next('/login');
    } else if (to.meta.permission) {
        username === 'admin'
            ? next()
            : next('/403');
    } else {
        next();   
    }
});

export default router;