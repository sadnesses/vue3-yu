import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import installElementPlus from './plugins/element'
import './assets/css/icon.css'
import yuIcon from './components/yu_icon.vue'
import VueClipboard from 'vue3-clipboard'
import moment from 'moment'


import VueMarkdownEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js';
import '@kangc/v-md-editor/lib/theme/style/vuepress.css';
import Prism from 'prismjs';

VueMarkdownEditor.use(vuepressTheme, {
    Prism,
});

const app = createApp(App)
//markdown
app.use(VueMarkdownEditor);

installElementPlus(app)
// app.config.globalProperties.$echarts = echarts
app.use(store)
    .use(router)
    .mount('#app')
app.component('yu-icon', yuIcon);
app.use(VueClipboard, {
    autoSetContainer: true,
    appendToBody: true,
})
app.config.globalProperties.$moment = moment
