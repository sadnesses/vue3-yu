import axios from 'axios';
import {useRouter} from "vue-router";

let baseURL = '/api'

const service = axios.create({
    baseURL: baseURL,
    timeout: 20000,
    withCredentials: true,  // send cookies when cross-domain requests
    headers: {
        // clear cors
        'Cache-Control': 'no-cache',
        'Pragma': 'no-cache'
    },
    changeOrigin: true,
});

const fileDown = axios.create({
    baseURL: baseURL,
    timeout: 20000,
    withCredentials: true,  // send cookies when cross-domain requests
    headers: {
        // clear cors
        'Cache-Control': 'no-cache',
        'Pragma': 'no-cache'
    },
    responseType: 'blob'
})

service.interceptors.request.use(
    config => {
        return config;
    },
    error => {
        console.log(error);
        return Promise.reject();
    }
);

service.interceptors.response.use(
    response => {
        if (response.status === 200) {
            if (response.data.code == 200) {

            } else {

            }
            return response.data;
        } else {
            Promise.reject();
        }
    },
    error => {
        return Promise.reject(error);
    }
);

export default service;
