import request from '../utils/request';
import fileDown from '../utils/request';

/**
 * 查询权限
 * @param query
 * @returns {AxiosPromise}
 */
export const fetchData = query => {
    return request({
        url: '/authority/data',
        method: 'get',
        params: query
    });
};

/**
 * 更新权限状态
 * @param query
 * @returns {AxiosPromise}
 */
export const updateStatus = query => {
    return request({
        url: '/authority/alertStatus',
        method: 'put',
        params: query
    });
};

/**
 * 更新权限
 * @param query
 * @returns {AxiosPromise}
 */
export const updateUrl = query => {
    return request({
        url: '/authority/alert',
        method: 'put',
        data: query
    });
}

/**
 * 创建接口权限
 * @param query
 * @returns {AxiosPromise}
 */
export const createUrl = query => {
    return request({
        url: '/authority/add',
        method: 'post',
        data: query
    });
}

/**
 * 删除接口权限
 * @param query
 * @returns {AxiosPromise}
 */
export const deleteUrl = query => {
    return request({
        url: '/authority/delete',
        method: 'delete',
        data: query
    });
}

/**
 * 文件下载
 * @param query
 * @returns {AxiosPromise}
 */
export const getLogFile = () => {
    return fileDown({
        url: '/system/logDown',
        method: 'get',
        responseType:'blob'
    })
}