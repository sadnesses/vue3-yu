import request from '../utils/request';

export const fetchSystemData = query => {
    return request({
        url: '/system/data',
        method: 'get',
        params: query
    });
};
