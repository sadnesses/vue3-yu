import request from '../utils/localrequest';

export const fetchData = query => {
    return request({
        url: './table.json',
        method: 'get',
        params: query
    });
};

export const fetchUrlData = () => {
    return request({
        url: './url.json',
        method: 'post'
    });
}


export const fetchEchartsData = () => {
    return request({
        url: './RisingSunChart.json',
        method: 'post'
    });
}

export const fetchIconData = () =>{
    return request({
        url: './icon.json',
        method: 'post'
    });
}

export const fetchHomeData = () =>{
    return request({
        url: './home.json',
        method: 'post'
    });
}

export const fetchyiyan = () =>{
    return request({
        url : 'https://v1.hitokoto.cn/?encode=json',
        method: 'get'
    })
}

export const fetchBigdata = () =>{
    return request({
        url : './life.json',
        method: 'post'
    })
}

export const fetchLinedata = () =>{
    return request({
        url : './line.json',
        method: 'post'
    })
}

export const fetchwordClouddata = () =>{
    return request({
        url : './mock.json',
        method: 'post'
    })
}