import request from '../utils/request';

export const fetchMDData = query => {
    return request({
        url: '/getMD',
        method: 'get'
    });
};

export const saveMDData = query => {
    return request({
        url: '/saveMD',
        method: 'post',
        data:query,
        headers: {"Content-Type": "text/plain"}
    });
};
