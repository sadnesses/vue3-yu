import vue from '@vitejs/plugin-vue'
export default {
    //项目部署的基础路径
    base: './',
    //插件
    plugins: [vue()],
    optimizeDeps: {
        // include: ['schart.js']
    },
    //服务
    server:{
        //服务器主机名
        host:"0.0.0.0",
        //端口号
        port: "3000",
        //设为 true 时若端口已被占用则会直接退出，
        //而不是尝试下一个可用端口
        strictPort: true,
        //自定义代理规则
        proxy: {
            // 选项写法
            '/api': {
                target: 'http://139.155.177.185:7777',
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, '')
            }
        },
        //开发服务器配置 CORS
        //boolean | CorsOptions
        cors: {

        },
        //设置为 true 强制使依赖预构建
        force: true,
        //禁用或配置 HMR 连接
        hmr: {

        },
        //传递给 chokidar 的文件系统监视器选项
        watch: {

        }
    }

}
