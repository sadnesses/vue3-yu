# vue3-yu

### demo：http://139.155.177.185/

采用**vue3.0+elmentPlus**

> 作者菜鸡一枚，**菜鸡！菜鸡！菜鸡！**重要的事说三遍😎😎😎
>
> 主要从事后端开发，该项目是上班摸鱼做的，为了学习vue3和前端知识。基于大佬的项目vue-manage-system开发的。感谢大佬们的开源（直接跳过脚手架的搭建🤣，以及其他麻烦的事）
>
> 有什么问题，不要问我，因为我也不会。如果你解决了，能分享吗？（我是真不会。这个项目出现的问题，都是百度解决的😁）

```
上班摸鱼学习是最快的事！！！！
```

修改内容为：

- elment-plus升级最新版
- 左侧菜单重写
- 菜单栏修改
- 头部修改
- 封装icon图标
- 图表替换为Echarts
- icon图标替换成第三方icon图标（iconfont）
- 新增vue3-clipboard粘贴复制
- 新增moment时间处理



### 日志

- 修复Echarts第二次加载无法显示问题
- 新增antv组件



### 提示

- ```
  打包会报整，但不影响运行，百度了下，说是elmentplus的问题
  rendering chunks (19)...warnings when minifying css:
   > <stdin>:300:0: warning: "@charset" must be the first rule in the file        
      300 │ @charset "UTF-8";:root{--el-color-white:#ffffff;--el-color-black:#0...
          ╵ ~~~~~~~~
     <stdin>:1:0: note: This rule cannot come before a "@charset" rule
        1 │ * {
          ╵ ^
  
  
  解决方法：
  在vite.config.js文件加上就能解决。不影响运行的！！！
    css: {
          postcss: {
              plugins: [
                  {
                      postcssPlugin: 'internal:charset-removal',
                      AtRule: {
                          charset: (atRule) => {
                              if (atRule.name === 'charset') {
                                  atRule.remove();
                              }
                          }
                      }
                  }
              ],
          },
      }
  ```

  